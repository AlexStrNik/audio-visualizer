#include <Wire.h>
#include <LiquidCrystal_I2C.h>

byte level0[] = {
    B00000,
    B00000,
    B00000,
    B00000,
    B00000,
    B00000,
    B00000,
    B11111};

byte level1[] = {
    B00000,
    B00000,
    B00000,
    B00000,
    B00000,
    B00000,
    B11111,
    B11111};

byte level2[] = {
    B00000,
    B00000,
    B00000,
    B00000,
    B00000,
    B11111,
    B11111,
    B11111};

byte level3[] = {
    B00000,
    B00000,
    B00000,
    B00000,
    B11111,
    B11111,
    B11111,
    B11111};

byte level4[] = {
    B00000,
    B00000,
    B00000,
    B11111,
    B11111,
    B11111,
    B11111,
    B11111};

byte level5[] = {
    B00000,
    B00000,
    B11111,
    B11111,
    B11111,
    B11111,
    B11111,
    B11111};

byte level6[] = {
    B00000,
    B11111,
    B11111,
    B11111,
    B11111,
    B11111,
    B11111,
    B11111};

byte level7[] = {
    B11111,
    B11111,
    B11111,
    B11111,
    B11111,
    B11111,
    B11111,
    B11111};

LiquidCrystal_I2C lcd(0x27, 16, 2);

void setup()
{
    lcd.init();
    lcd.backlight();
    lcd.noCursor();

    lcd.createChar(0, level0);
    lcd.createChar(1, level1);
    lcd.createChar(2, level2);
    lcd.createChar(3, level3);
    lcd.createChar(4, level4);
    lcd.createChar(5, level5);
    lcd.createChar(6, level6);
    lcd.createChar(7, level7);

    Serial.begin(250000);
}

void draw_bar(int pos, int value)
{
    if (value == 0)
    {
        lcd.setCursor(pos, 1);
        lcd.write(' ');
        lcd.setCursor(pos, 0);
        lcd.write(' ');
    }
    else if (value > 8)
    {
        lcd.setCursor(pos, 1);
        lcd.write(byte(7));
        lcd.setCursor(pos, 0);
        lcd.write(byte(value - 9));
    }
    else
    {
        lcd.setCursor(pos, 1);
        lcd.write(byte(value - 1));
        lcd.setCursor(pos, 0);
        lcd.write(' ');
    }
}

void loop()
{
    String values = Serial.readStringUntil('\n');
    
    for (int i = 0; i < 16; i++)
    {
        int value = (int)values[i] - 48;
        value = max(value, 1);
        draw_bar(i, value);
    }
}
