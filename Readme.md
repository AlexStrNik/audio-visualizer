# audio-visualizer

![](media/intro.gif)

Simple ~~but powerful~~ audio visualizer. 
Built with Arduino Mega 2560 and LCD1602 (I2C).

## Arduino Setup
- Connect LCD1602 Display with pin headers (I2C)
- Upload `visualizer.ino` sketch
- Connect Arduino to your PC 

## PC Setup
- Run client in terminal (Only Windows supported yet)
- Select COM port which Arduino connected
- Turn on music and enjoy :)