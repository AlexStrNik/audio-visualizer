﻿using Microsoft.VisualBasic;
using NAudio.Wave;
using System;
using System.Diagnostics;
using System.IO.Ports;

namespace WinClient
{
    class Program
    {
        private IWaveIn waveIn;
        private SampleAggregator _sampleAggregator = new SampleAggregator(128);
        private SerialPort _serialPort;

        static void Main(string[] args)
        {
            new Program().Init();
        }

        void Init()
        {
            string[] ports = SerialPort.GetPortNames();

            Console.WriteLine("Select COM:");

            for (int i = 0; i < ports.Length; i++)
            {
                Console.WriteLine("[" + i.ToString() + "] " + ports[i].ToString());
            }

            _serialPort = new SerialPort();
            _serialPort.PortName = ports[int.Parse(Console.ReadLine())];
            _serialPort.BaudRate = 250000;
            _serialPort.Open();

            _sampleAggregator.FftCalculated += new EventHandler<FftEventArgs>(FftCalculated);
            _sampleAggregator.PerformFFT = true;

            waveIn = new WasapiLoopbackCapture();
            waveIn.DataAvailable += WaveIn_DataAvailable;

            waveIn.StartRecording();
        }

        void WaveIn_DataAvailable(object sender, WaveInEventArgs e)
        {
            byte[] buffer = e.Buffer;
            int bytesRecorded = e.BytesRecorded;
            int bufferIncrement = waveIn.WaveFormat.BlockAlign;

            for (int index = 0; index < bytesRecorded; index += bufferIncrement)
            {
                float sample32 = BitConverter.ToSingle(buffer, index);
                this._sampleAggregator.Add(sample32);
            }
        }

        void FftCalculated(object sender, FftEventArgs e)
        {
            string result = "";
            for(int i = 0; i < 128; i+=16)
            {
                float r = 0;

                for (int j = 0; j < 16; j++)
                {
                    r += e.Result[i + j].X;
                }

                r /= 16F;
                r = Math.Abs(r) * 10000;
                r = Math.Min(r, 16);
                int c = 48 + (int)Math.Round(r);

                result += (char)c;
            }

            _serialPort.WriteLine(result);
        }
    }
}
